#+xcvb (module (:description "package for QUUX time"))

(cl:defpackage :quux-time
  (:use :common-lisp) ;;  :alexandria ? quux-macros?
  (:shadow
   #:decode-universal-time
   #:encode-universal-time)
  (:export
   :integer-date
   :integer-tofd
   :integer-time
   :integer-duration
   ;; XXX... insert here a list of everything exported...
   ))
